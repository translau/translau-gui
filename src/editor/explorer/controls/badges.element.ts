import { bindable } from 'aurelia-framework';

export class BadgesCustomElement {
    @bindable public missing: {
        translations: number;
        languages?: number;
    } = { translations: 0 };
}
