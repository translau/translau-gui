import { bindable, containerless, customElement } from 'aurelia-framework';
import { Folder } from 'model/folder.model';
import { Namespace } from 'model/namespace.model';
import { showNotification } from 'model/notifications/notification.model';
import { Translations } from 'model/translations.model';
import * as uikit from 'uikit';

@containerless()
@customElement('item-control')
export class ItemControl {

    @bindable public item: Folder | Namespace;
    @bindable public data: Translations;

    protected async addKey(item: Folder | Namespace): Promise<void> {
        try {
            const key: string | null = await uikit.modal.prompt('Translation key to add:', '');
            if (key === null) {
                return;
            }
            item.addKey(key, this.data.languages);
            showNotification(
                `<span uk-icon="icon: check"></span> <b>${key}</b> added.`,
                { status: 'success' }
            );
        } catch (error) {
            console.error(error);
            showNotification(
                `<span uk-icon="icon: ban"></span> ${error.message}`,
                { status: 'danger', timeout: 0 }
            );
        }
    }

    protected async removeKey(item: Folder | Namespace): Promise<void> {
        try {
            const key: string | null = await uikit.modal.prompt('Translation key to remove:', '');
            if (key === null) {
                return;
            }
            item.removeKey(key);
            showNotification(
                `<span uk-icon="icon: check"></span> <b>${key}</b> removed.`,
                { status: 'success' }
            );
        } catch (error) {
            console.error(error);
            showNotification(
                `<span uk-icon="icon: ban"></span> ${error.message}`,
                { status: 'danger', timeout: 0 }
            );
        }
    }

    protected async addNamespace(item: Folder | Namespace): Promise<void> {
        try {
            const key: string | null = await uikit.modal.prompt('Namespace key to add:', '');
            if (key === null) {
                return;
            }
            item.addNamespace(key, this.data.languages);
            showNotification(
                `<span uk-icon="icon: check"></span> <b>${key}</b> added.`,
                { status: 'success' }
            );
        } catch (error) {
            console.error(error);
            showNotification(
                `<span uk-icon="icon: ban"></span> ${error.message}`,
                { status: 'danger', timeout: 0 }
            );
        }
    }

    protected async removeNamespace(item: Folder | Namespace): Promise<void> {
        try {
            const key: string | null = await uikit.modal.prompt('Namespace key to remove:', '');
            if (key === null) {
                return;
            }
            item.removeNamespace(key);
            showNotification(
                `<span uk-icon="icon: check"></span> <b>${key}</b> removed.`,
                { status: 'success' }
            );
        } catch (error) {
            console.error(error);
            showNotification(
                `<span uk-icon="icon: ban"></span> ${error.message}`,
                { status: 'danger', timeout: 0 }
            );
        }
    }

}
