import { bindable, containerless } from 'aurelia-framework';
import { Folder } from 'model/folder.model';
import { Namespace } from 'model/namespace.model';
import { Translations } from 'model/translations.model';

@containerless()
export class FoldersCustomElement {

    @bindable public data: Translations;
    @bindable public folders: Folder[] = [];
    @bindable public selectedNamespace: Namespace | null = null;

}
