import { inject } from 'aurelia-framework';
import { Namespace } from 'model/namespace.model';
import { showNotification } from 'model/notifications/notification.model';
import { Translations as TranslationsModel } from 'model/translations.model';
import { Translations } from 'services/translations.service';

@inject(Translations)
export class Editor {

    protected translationsModel: TranslationsModel = new TranslationsModel();
    protected selectedNamespace: Namespace | null = null;

    public constructor(
        private readonly translationsService: Translations
    ) { }

    protected async save(): Promise<void> {
        try {
            console.log(this.translationsModel.extract());
            await this.translationsService.postAll(this.translationsModel.extract());
            showNotification(
                '<span uk-icon="icon: check"></span> Successfully saved.',
                { status: 'success' }
            );
        } catch (error) {
            showNotification(
                `<span uk-icon="icon: ban"></span> Something went wrong while saving data.
                <p><b>Error:</b> ${error.message}</p>`,
                { status: 'danger', timeout: 0 }
            );
            console.error(error);
        }
    }

    protected async build(rebuild: boolean = false): Promise<void> {
        try {
            const data: any = await this.translationsService.getAll();
            if (rebuild) {
                this.translationsModel.rebuild(data);
                this.selectedNamespace = null;
                showNotification(
                    '<span uk-icon="icon: check"></span> Successfully reloaded.',
                    { status: 'success' }
                );
            } else {
                this.translationsModel.build(data);
            }
        } catch (error) {
            showNotification(
                `<span uk-icon="icon: ban"></span> Something went wrong while loading data.
                <p><b>Error:</b> ${error.message}</p>`,
                { status: 'danger', timeout: 0 }
            );
        }
    }

    public async activate(): Promise<void> {
        await this.build();
        console.log('TCL: Editor -> this.translationsModel', this.translationsModel);
    }

}
