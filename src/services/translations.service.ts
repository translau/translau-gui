import environment from 'environment';

import { IFolder } from './folder.interface';
import { AService } from './service.abstract';

export class Translations extends AService {

    private readonly apiPath: string = `${this.api}/translations`;
    private static readonly host: string = environment.host;

    public async getAll(): Promise<IFolder> {
        const response: Response = await this.http.get(
            `${Translations.host}/${this.apiPath}`
        );
        if (!response.ok) {
            const message: { error: string } = await response.json();
            throw new Error(message.error);
        }
        return await response.json();
    }

    public async postAll(body: IFolder): Promise<void> {
        const response: Response = await this.http.post(
            `${Translations.host}/${this.apiPath}`,
            JSON.stringify(body)
        );
        if (!response.ok) {
            const message: { error: string } = await response.json();
            throw new Error(message.error);
        }
    }

}
