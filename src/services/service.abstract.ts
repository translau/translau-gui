import { HttpClient } from 'aurelia-fetch-client';
import { inject } from 'aurelia-framework';

@inject(HttpClient)
export abstract class AService {

    protected readonly api: string = 'api';

    public constructor(
        protected readonly http: HttpClient
    ) { }

}
