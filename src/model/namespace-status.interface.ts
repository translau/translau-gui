export interface INamespaceStatus {
    missing: {
        translations: number;
    };
}
