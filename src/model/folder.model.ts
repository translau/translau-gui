import { IData } from 'services/data.interface';
import { IFile } from 'services/file.interface';
import { IFolder } from 'services/folder.interface';

import { IFolderStatus } from './folder-status.interface';
import { Namespace } from './namespace.model';

export class Folder {
    public name: string = '';
    public folders: Folder[] = [];
    public namespace: Namespace | null = null;
    public languages: Set<string> = new Set();
    public status: IFolderStatus = {
        missing: {
            translations: 0,
            languages: 0
        }
    };

    private buildFolders(folders: IFolder[]): void {
        if (!folders) {
            return;
        }
        for (const folder of folders) {
            const addedFolder: Folder = new Folder();
            const languages: Set<string> = addedFolder.build(folder);
            this.languages = new Set([...this.languages, ...languages]);
            this.folders.push(addedFolder);
        }
    }

    private extractFolders(): IFolder[] {
        const folders: IFolder[] = [];
        for (const folder of this.folders) {
            folders.push(folder.extract());
        }
        return folders;
    }

    private buildNamespaces(files: IFile[]): void {
        if (!files) {
            return;
        }
        this.namespace = new Namespace();
        for (const file of files) {
            this.namespace.addFile(file.language, file.data);
            this.languages.add(file.language);
        }
    }

    private extractNamespaces(): IFile[] {
        const files: IFile[] = [];
        for (const language of this.languages) {
            files.push(this.namespace.extractFile(language));
        }
        return files;
    }

    public build(data: IFolder): Set<string> {
        this.name = data.name;
        this.buildFolders(data.subFolders);
        this.buildNamespaces(data.files);
        return this.languages;
    }

    public postProcess(languages: Set<string>): number {
        this.status.missing.languages = languages.size - this.languages.size;
        this.languages = languages;
        for (const folder of this.folders) {
            this.status.missing.translations += folder.postProcess(languages);
        }
        if (this.namespace) {
            this.status.missing.translations += this.namespace.postProcess(languages);
        }
        return this.status.missing.translations;
    }

    public extract(): IFolder {
        const folder: IFolder = {
            name: this.name
        };
        if (this.folders.length > 0) {
            folder.subFolders = this.extractFolders();
        }
        if (this.namespace !== null) {
            folder.files = this.extractNamespaces();
        }
        return folder;
    }

    public editLanguage(original: string, code: string | null): void {
        for (const folder of this.folders) {
            folder.editLanguage(original, code);
        }
        if (this.namespace) {
            this.namespace.editLanguage(original, code);
        }
    }

    public addFolder(key: string, languages: Set<string>): void {
        if (this.folders) {
            const folder: Folder = new Folder();
            folder.build({ name: key });
            this.folders.push(folder);
        }
    }

    public addKey(key: string, languages: Set<string>): void {
        if (this.namespace) {
            this.namespace.addKey(key, languages);
        }
    }

    public removeKey(key: string): void {
        if (this.namespace) {
            this.namespace.removeKey(key);
        }
    }

    public addNamespace(key: string, languages: Set<string>): void {
        if (this.namespace) {
            this.namespace.addNamespace(key, languages);
        }
    }

    public removeNamespace(key: string): void {
        if (this.namespace) {
            this.namespace.removeNamespace(key);
        }
    }
}
