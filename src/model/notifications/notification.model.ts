import * as UIkit from 'uikit';

export function showNotification(text: string, options: any): void {
    UIkit.notification(text, options);
}
