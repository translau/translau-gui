import { IFolder } from '../services/folder.interface';
import { Folder } from './folder.model';

export class Translations {
    public folder: Folder;
    public languages: Set<string>;
    public missing: number;

    public constructor() {
        this.init();
    }

    private init(): void {
        this.folder = new Folder();
        this.languages = new Set();
        this.missing = 0;
    }

    private validLanguageCode(code: string): void {
        if (code === '') {
            throw new Error("Language code can't be empty.");
        }
        if (this.languages.has(code)) {
            throw new Error(`Language code '${code}' already exists.`);
        }
    }

    public rebuild(data: IFolder): void {
        this.init();
        this.build(data);
    }

    public build(data: IFolder): void {
        this.languages = this.folder.build(data);
        this.missing = this.folder.postProcess(this.languages);
    }

    public extract(): IFolder {
        return this.folder.extract();
    }

    public addLanguage(code: string): void {
        this.validLanguageCode(code);
        this.languages.add(code);
        this.folder.postProcess(this.languages);
    }

    public editLanguage(original: string, code: string | null = null): void {
        this.validLanguageCode(code);
        this.languages.delete(original);
        if (code !== null) {
            this.languages.add(code);
        }
        this.folder.editLanguage(original, code);
    }
}
