import { Aurelia } from 'aurelia-framework';
import { PLATFORM } from 'aurelia-pal';
import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';

import environment from './environment';

import 'core-js/stable';
import 'uikit/dist/css/uikit.min.css';

export function configure(aurelia: Aurelia) {
  aurelia.use
    .standardConfiguration()
    .feature(PLATFORM.moduleName('resources/index'));

  aurelia.use.developmentLogging(environment.debug ? 'debug' : 'warn');

  if (environment.testing) {
    aurelia.use.plugin(PLATFORM.moduleName('aurelia-testing'));
  }

  UIkit.use(Icons);

  aurelia.start().then(() => aurelia.setRoot(PLATFORM.moduleName('app')));
}
