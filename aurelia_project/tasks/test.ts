import * as path from 'path';

import { CLIOptions } from 'aurelia-cli';
import * as jest from 'jest-cli';

import * as jestConfig from '../../jest.config';

export default (cb) => {
    const options: any = jestConfig;

    if (CLIOptions.hasFlag('watch')) {
        Object.assign(options, { watchAll: true });
    }

    jest.runCLI(options, [path.resolve(__dirname, '../../')]).then(({ results }) => {
        if (results.numFailedTests || results.numFailedTestSuites) {
            cb('Tests Failed');
        } else {
            cb();
        }
    });
};
