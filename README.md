# Translau

GUI for translation manager tool [Translau](https://translau.gitlab.io)

## Instalation

```bash
npm install
```

## Run dev app

Run `start`, then open `http://localhost:8080`
```bash
npm run start
```

## Build

```bash
npm run build
npm run build:prod # for production
```

## Unit tests

```bash
npm test
npm run test:watch # for watching changes
```
